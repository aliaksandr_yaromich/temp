import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main
{
    public static void main(String[] args) throws Exception
    {
        String xml = getResourceContent("test.xml");
        String xslt = getResourceContent("test.xsl");
        Transformer transformer = TransformerFactory.newInstance().newTransformer(new StreamSource(new StringReader(xslt)));
        Writer transformedXml = new StringWriter();
        Result result = new StreamResult(transformedXml);
        transformer.transform(new StreamSource(new StringReader(xml)), result);
        System.out.println(transformedXml.toString());
    }
    
    private static String getResourceContent(String fileName) throws Exception
    {
        return new String(Files.readAllBytes(Paths.get(Main.class.getResource(fileName).toURI())));
    }
}
