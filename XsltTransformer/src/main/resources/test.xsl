<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ext="http://exslt.org/common">
    <xsl:output method="xml" indent="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:template match="@*|node()">
        <xsl:copy>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>

    <!-- node mask -->
    <xsl:template match="//element2/text()">*****</xsl:template>
    
    <!-- attribute mask -->
    <xsl:template match="//element1/@attr1">
        <xsl:attribute name="attr1">*****</xsl:attribute>
    </xsl:template>
    
</xsl:stylesheet>